﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObsPointsControl.Model.Twitch;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ObsPointsControl.Model
{
	public class Config
	{
		public static string DefaultConfigFile { get { return "config.json"; } }

		public bool FirstRun = true;
		public string ObsAddr = "127.0.0.1";
		public ushort ObsPort = 4444;
		public string ObsAuth = null;
		public string Hotkey = "Shift+F5";
		private string _twitch_user = null;
		private string _twitch_user_id = null;

		public string TwitchUser {
			get { return _twitch_user; }
			set
			{
				if (value != _twitch_user)
				{
					_twitch_user_id = null;
				}
				_twitch_user = value;
			}
		}
		public string TwitchUserId
		{
			get
			{
				if (_twitch_user_id == null && _twitch_user != null)
				{
					string val;
					if (TwitchIdResolver.ResolveLogins("vde21u18dna92iwn9z5yzmyqwdpthq", _twitch_user).TryGetValue(_twitch_user, out val))
					{
						_twitch_user_id = val;
					}
					else { _twitch_user_id = null; };
				}
				return _twitch_user_id;
			}
		}

		public string TwitchAuth = null;
		public List<Action> Actions = new List<Action>();

		public void Store(string file = null)
		{
			using (StreamWriter sw = new StreamWriter(File.Create(file ?? DefaultConfigFile)))
			{
				using (JsonWriter jw = new JsonTextWriter(sw))
				{
					var js = new JsonSerializer();
					js.NullValueHandling = NullValueHandling.Include;
					js.Serialize(jw, new JObject(
						new JProperty("obs_addr", ObsAddr),
						new JProperty("obs_port", ObsPort),
						new JProperty("obs_auth", ObsAuth),
						new JProperty("first_run", FirstRun),
						new JProperty("twitch_user", TwitchUser),
						new JProperty("twitch_user_id", TwitchUserId),
						new JProperty("twitch_auth", TwitchAuth),
						new JProperty("pause_hotkey", Hotkey),
						new JProperty("actions", new JArray(
							from action in Actions
							select action.ToJson())))); ;
				}
			}
		}
		public static Config Load(string file = null)
		{
			MainWnd.Log("Loading config...");
			var f = file ?? DefaultConfigFile;
			if (!File.Exists(f))
			{
				MainWnd.Log("Missing config: creating fresh one");
				var nc = new Config();
				nc.Store(f);
				return nc;
			}

			using (StreamReader sr = new StreamReader(File.OpenRead(f)))
			{
				using (JsonReader jr = new JsonTextReader(sr))
				{
					JObject o = (JObject)JToken.ReadFrom(jr);
					Config cfg = new Config
					{
						FirstRun = (bool)o["first_run"],
						ObsAddr = (string)o["obs_addr"],
						ObsPort = (ushort)o["obs_port"],
						ObsAuth = (string)o["obs_auth"],
						Hotkey = (string)o["pause_hotkey"],
						_twitch_user = (string)o["twitch_user"],
						_twitch_user_id = (string)o["twitch_user_id"],
						TwitchAuth = (string)o["twitch_auth"],
						Actions = (from obj in o["actions"]
								   select Action.FromJsonS((JObject)obj)).ToList()
					};
					return cfg;
				}
			}
		}
	}
}
