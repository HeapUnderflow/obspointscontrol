﻿namespace ObsPointsControl.Model
{
	public enum ConnectionResult
	{
		Ok, AuthFailed, ConnectionFailed
	}
}
