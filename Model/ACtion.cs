﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;

namespace ObsPointsControl.Model
{
	public class Action	: IJsonConvertable<Action>
	{
		// Trigger
		public uint PointCount;

		// Response
		public string Target;
		public TargetType Type;
		public ulong Duration = 1000;
		public ulong Timeout = 0;
		public bool Value;
		public string Parent;
		public Guid Id { get; private set; }

		public Action(string target, TargetType type, uint pcount, bool value = true, string parent = null, ulong duration = 1000, ulong timeout = 0, Guid? id = null)
		{
			Target = target;
			Type = type;
			Duration = duration;
			Timeout = timeout;
			PointCount = pcount;
			Value = value;
			Id = id ?? Guid.NewGuid();
			Parent = parent;
		}

		public static Action FromJsonS(JObject j)
		{
			var id = new Guid((string)j["id"]);
			var target = (string)j["target"];
			var type = ((string)j["type"]).ToTargetType() ?? TargetType.Source;
			var duration = j.Value<ulong>("duration");
			var timeout = j.Value<ulong>("timeout");
			var pointcount = j.Value<uint>("point_count");
			var value = j.Value<bool>("value");
			var parent = (string)j["parent"];
			return new Action(target, type, pointcount, value, parent, duration, timeout, id);
		}

		public Action FromJson(JObject j)
		{
			return FromJsonS(j);
		}

		public JObject ToJson()
		{
			return new JObject(
				new JProperty("id", Id),
				new JProperty("point_count", PointCount),
				new JProperty("target", Target),
				new JProperty("type", Type.ToJsonString()),
				new JProperty("duration", Duration),
				new JProperty("timeout", Timeout),
				new JProperty("parent", Parent),
				new JProperty("value", Value));
		}

		public void Dbg()
		{
			Debug.WriteLine($"Action[Id = {Id}]");
			Debug.Indent();

			Debug.WriteLine(Type, "Type");
			Debug.WriteLine(Target, "Target");
			Debug.WriteLine(Parent ?? "null", "Parent");
			Debug.WriteLine(PointCount, "PointCount");
			Debug.WriteLine(Duration, "Duration");
			Debug.WriteLine(Timeout, "Timeout");
			Debug.WriteLine(Value, "Value");

			Debug.Unindent();
		}
	}
}
