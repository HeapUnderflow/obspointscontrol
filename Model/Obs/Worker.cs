﻿using OBSWebsocketDotNet;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace ObsPointsControl.Model.Obs
{
	class Worker
	{
		/// <summary>
		/// The idle-time the thread sleeps on busy loop
		/// </summary>
		public int BusyLoopDelay = 10;

		private Thread worker;
		private OBSWebsocket obs;

		private volatile bool paused = false;
		private volatile bool shouldExit = false;

		private ConcurrentQueue<Event> events = new ConcurrentQueue<Event>();


		public Worker(ref OBSWebsocket ows)
		{
			obs = ows;
			worker = new Thread(Run)
			{
				Name = "ObsThreadWorker" + GetHashCode()
			};
			worker.Start();
			worker.IsBackground = true;
		}

		/// <summary>
		/// Request that the worker thread exits asap
		/// </summary>
		public void RequestExit()
		{
			shouldExit = true;
		}

		/// <summary>
		/// Push a new event to the queue
		/// </summary>
		/// <param name="ev"></param>
		public void PushEvent(Event ev)
		{
			ev.Dbg();
			events.Enqueue(ev);
		}

		/// <summary>
		/// (Un)pause even handling
		/// </summary>
		/// <param name="pause">Pause or Unpause</param>
		public void SetPaused(bool pause)
		{
			paused = pause;
		}

		/// <summary>
		/// Run Loop
		/// </summary>
		private void Run() {
			while (!shouldExit && !obs.IsConnected)
			{
				Thread.Sleep(10);
			}

			Thread.Sleep(1000);

			while (!shouldExit && obs.IsConnected)
			{
				if (paused)
				{
					PausedRun();
					continue;
				}

				if (!HandleNextEvent())
				{
					Thread.Sleep(BusyLoopDelay);
				}
			}
		}

		/// <summary>
		/// Internal loop that stops handling while paused
		/// </summary>
		private void PausedRun()
		{
			HandleNextEvent(true);

			while (paused)
			{
				Thread.Sleep(BusyLoopDelay);
			}
		}

		/// <summary>
		/// Handles a queued event (either starting it, or stopping it if its time is over)
		/// </summary>
		/// <param name="runningOnly">Only handle a currently running event</param>
		/// <returns>If a event was processed</returns>
		public bool HandleNextEvent(bool runningOnly = false)
		{
			if (events.TryPeek(out Event ev))
			{
				if (ev.StartedAt != null)
				{
					var delay = DateTime.UtcNow - ev.StartedAt;
					if (delay < ev.ResetAfter)
					{
						Thread.Sleep((int)(ev.ResetAfter - delay)?.TotalMilliseconds);
					}
					ResetEvent(ev);
					if (!events.TryDequeue(out ev))
					{
						MainWnd.Log("OBS-WARN: Could not remove event (queue empty)");
					}
				} else if (!runningOnly)
				{
					ev.Start();
					SetEvent(ev);
				} else
				{
					return false;
				}
				return true;
			}
			return false;
		}

		private void SetEvent(Event ev)
		{
			var currentScene = obs.GetCurrentScene().Name;
			switch (ev.Type)
			{
				case TargetType.Scene:
					ev.SetResetScene(ev.Parent ?? currentScene);
					obs.SetCurrentScene(ev.Target);
					break;
				case TargetType.Source:
					obs.SetSourceRender(ev.Target, ev.SourceIsSetActive, ev.Parent ?? currentScene);
					break;
			}
		}

		private void ResetEvent(Event ev)
		{
			switch (ev.Type)
			{
				case TargetType.Scene:
					obs.SetCurrentScene(ev.SceneResetTo);
					break;
				case TargetType.Source:
					obs.SetSourceRender(ev.Target, !ev.SourceIsSetActive, ev.Parent);
					break;
			}
		}
	}
}
