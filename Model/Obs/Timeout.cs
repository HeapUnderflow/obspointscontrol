﻿using System;

namespace ObsPointsControl.Model.Obs
{
	class Timeout
	{
		public DateTime InvalidAfter { get; private set; }
		public Guid Id { get; private set; }

		public Timeout(Guid id, ulong ivafter = 0)
		{
			Id = id;
			InvalidAfter = DateTime.UtcNow + TimeSpan.FromMilliseconds(ivafter);
		}
	}
}
