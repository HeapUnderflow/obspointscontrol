﻿using ObsPointsControl.Model;
using OBSWebsocketDotNet;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace ObsPointsControl.Model.Obs
{
	public class ObsController
	{
		public string Url;
		public ushort Port;
		public string Auth;

		public event EventHandler OnObsConnected;
		public event EventHandler OnObsDisconnected;

		private OBSWebsocket obs;
		private Worker wrk;
		private List<Timeout> timeouts = new List<Timeout>();

		public ObsController(string url = "127.0.0.1", ushort port = 4444, string auth = "")
		{
			obs = new OBSWebsocket();
			obs.Connected += (s, e) => OnObsConnected(this, e);
			obs.Disconnected += (s, e) => OnObsDisconnected(this, e);
			obs.SceneChanged += OnSceneChanged;
			Url = url ?? "127.0.0.1";
			Port = port;
			Auth = auth ?? "";
			wrk = new Worker(ref obs);
		}

		public void Disconnect()
		{
			if (!obs.IsConnected) { return; }
			wrk.RequestExit();
			obs.Disconnect();
		}

		public void SetPaused(bool paused)
		{
			wrk.SetPaused(paused);
		}

		private void OnSceneChanged(OBSWebsocket sender, string newSceneName)
		{
			// TODO: Implement cancelation if possible
		}

		public ConnectionResult Connect()
		{
			if (obs.IsConnected) { return ConnectionResult.Ok; }
			try
			{
				obs.Connect("ws://" + Url + ":" + Port, Auth);
				return ConnectionResult.Ok;
			}
			catch (AuthFailureException)
			{
				MainWnd.Log("OBS: Authentication Failure");
				return ConnectionResult.AuthFailed;
			}
			catch (ErrorResponseException ex)
			{
				MainWnd.Log("OBS: Connection Error: " + ex.Message);
				return ConnectionResult.ConnectionFailed;
			}
		}

		public void PushAction(in Action action)
		{
			if (CheckTimeout(action.Id)) { return; }
			switch (action.Type)
			{
				case TargetType.Scene:
					wrk.PushEvent(new Event(TargetType.Scene, action.Target, action.Parent, null, action.Duration, id: action.Id));
					break;
				case TargetType.Source:
					wrk.PushEvent(new Event(TargetType.Source, action.Target, action.Parent, null, action.Duration, action.Value, action.Id));
					break;
			}
			AddTimeout(action.Timeout, action.Id);
		}

		private bool CheckTimeout(Guid id)
		{
			var to = timeouts.Find(t => t.Id == id);
			if (to == null) { return false; }
			if (DateTime.UtcNow < to.InvalidAfter) { return true; }
			timeouts.Remove(to);
			return false;
		}

		private void AddTimeout(ulong timeout, Guid? id = null)
		{
			if (timeout == 0) { return; }
			timeouts.Add(new Timeout(id ?? Guid.NewGuid(), timeout));
		}
	}
}
