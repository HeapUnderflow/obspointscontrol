﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObsPointsControl.Model.Obs
{
	class Event
	{
		public Guid Id { get; private set; }
		public TargetType Type { get; private set; }
		public string Target { get; private set; }
		public string Parent { get; private set; }

		public bool SourceIsSetActive { get; private set; }

		public string SceneResetTo { get; private set; }

		// Creation Info
		public DateTime? StartedAt { get; private set; }
		public TimeSpan ResetAfter { get; private set; }

		public object Extra;

		public Event(
			TargetType type,
			string target,
			string parent = null,
			DateTime? created = null,
			ulong resetAfter = 0,
			bool source_isSetActive = true,
			Guid? id = null,
			object extra = null
			)
		{
			Type = type;
			Target = target;
			Parent = parent;
			StartedAt = created;
			ResetAfter = TimeSpan.FromMilliseconds(resetAfter);
			SourceIsSetActive = source_isSetActive;
			Id = id ?? Guid.NewGuid();
			Extra = extra;
		}

		public void SetResetScene(string scene)
		{
			SceneResetTo = scene;
		}

		public void Start(DateTime? started = null)
		{
			StartedAt = started ?? DateTime.UtcNow;
		}

		public void Dbg()
		{
			Debug.WriteLine("Event[Id = {0}]", Id);

			// Event
			Debug.Indent();
			Debug.WriteLine(Type, "Type");
			Debug.WriteLine(Target, "Target");
			Debug.WriteLine(Parent ?? "null", "Parent");
			Debug.WriteLine(StartedAt, "StartedAt");
			Debug.WriteLine(ResetAfter, "ResetAfter");

			// Event->Scene
			Debug.WriteLine("[Scene]");
			Debug.Indent();
			Debug.WriteLine(SceneResetTo ?? "null", "ResetTo");
			Debug.Unindent();

			// Event->Source
			Debug.WriteLine("[Source]");
			Debug.Indent();
			Debug.WriteLine(SourceIsSetActive, "IsSetActive");
			Debug.Unindent();
			Debug.WriteLine(Extra ?? "null", "Extra");

			Debug.Unindent();
		}
	}
}
