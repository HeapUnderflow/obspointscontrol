﻿using Newtonsoft.Json.Linq;

namespace ObsPointsControl.Model
{
	public interface IJsonConvertable<T>
	{
		JObject ToJson();
		T FromJson(JObject j);
	}
}
