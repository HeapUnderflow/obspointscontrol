﻿using System;

namespace ObsPointsControl.Model
{
	public static class Extensions
	{
		public static string ToJsonString(this TargetType tt)
		{
			return tt switch
			{
				TargetType.Scene => "scene",
				TargetType.Source => "source",
				_ => throw new NotImplementedException(string.Format("option {0} was not implemented", tt)),
			};
		}

		public static TargetType? ToTargetType(this string s)
		{
			return s switch
			{
				"scene" => TargetType.Scene,
				"source" => TargetType.Source,
				_ => new TargetType?(),
			};
		}

		public static string ToDisplayStrimg(this TargetType tt)
		{
			return tt switch
			{
				TargetType.Scene => "Scene",
				TargetType.Source => "Source",
				_ => throw new NotImplementedException(string.Format("option {0} was not implemented", tt)),
			};
		}

		public static TargetType? TTFromDisplayString(this string s)
		{
			return s switch
			{
				"Scene" => TargetType.Scene,
				"Source" => TargetType.Source,
				_ => new TargetType?()
			};
		}

		public static string NullOnEmpty(this string s)
		{
			if (s.Length == 0) { return null; }
			return s;
		}
	}
}
