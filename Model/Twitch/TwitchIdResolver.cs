﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ObsPointsControl.Model.Twitch
{
	static class TwitchIdResolver
	{
		private static readonly string Endpoint = "https://api.twitch.tv/helix";
		private static RestClient client = new RestClient(Endpoint);

		static TwitchIdResolver()
		{
			client.AddDefaultHeader("Accept", "application/json");
			client.AddDefaultHeader("AcceptEncoding", "utf-8");
		}

		/// <summary>
		/// Resolve a list of logins to their respective ids, using a clientid (application id)
		/// </summary>
		/// <param name="clientid">Client Authentication Token (Application ID)</param>
		/// <param name="logins">List of logins to resolve</param>
		/// <returns>Dict of resolved logins, mapped to their name</returns>
		public static Dictionary<string, string> ResolveLogins(string clientid, params string[] logins)
		{
			if (logins.Length > 100)
			{
				throw new ArgumentException("A maxium of 100 logins can be processed at a time", "logins");
			}
			var req = new RestRequest("users", Method.GET);
			req.AddHeader("Client-ID", clientid);

			foreach (var lg in logins)
			{
				req.AddParameter("login", lg);
			}

			IRestResponse resp = client.Execute(req);
			if (resp.IsSuccessful)
			{
				var o = (JObject)JToken.Parse(resp.Content);
				var res = new Dictionary<string, string>();
				foreach (var item in o["data"].ToArray())
				{
					res.Add((string)item["login"], (string)item["id"]);
				}
				return res;
			}
			else
			{
				MainWnd.Log($"Failed to resolve user ids: {resp.StatusCode} {resp.ErrorMessage}");
				return new Dictionary<string, string>();
			}
		}
	}
}
