﻿using System;
using TwitchLib.PubSub;
using TwitchLib.PubSub.Events;

namespace ObsPointsControl.Model.Twitch
{
	class TwitchController
	{
		public event EventHandler OnConnected;
		public event EventHandler OnDisconnected;
		public event EventHandler<OnRewardRedeemedArgs> OnReward;

		private TwitchPubSub client;
		public string Uid;
		public string Auth;

		private bool connected = false;

		public TwitchController(string uid, string auth = null)
		{
			// PubSub Oauth scope needed: channel:read:redemptions
			client = new TwitchPubSub();
			client.OnRewardRedeemed += OnRewardRedeemded;
			client.OnListenResponse += OnListenResponse;
			client.OnPubSubServiceConnected += (s, e) => OnConnected.Invoke(this, e);
			client.OnPubSubServiceConnected += OnServiceConnected;
			client.OnPubSubServiceClosed += (s, e) => OnDisconnected.Invoke(this, e);
			client.OnLog += (s, e) => MainWnd.Log($"dbg -> {e.Data}");

			this.Uid = uid;
			this.Auth = auth;
		}

		public void Connect()
		{
			if (Uid == null) { return; };
			if (connected) { return; }
			connected = true;
			client.Connect();
		}

		private void OnServiceConnected(object sender, EventArgs e)
		{
			client.ListenToRewards(Uid);
			client.SendTopics(oauth: Auth);
		}

		public void Disconnect()
		{
			if (!connected) { return; }
			connected = false;
			client.SendTopics(oauth: Auth, true);
			client.Disconnect();
		}

		private void OnListenResponse(object sender, OnListenResponseArgs e)
		{
			if (!e.Successful)
			{
				MainWnd.Log("TWITCH: Failed to listen to twitch events");
				MainWnd.Log($"TWITCH: {e.Response.Error}");
			} else
            {
				MainWnd.Log($"TWITCH: Listening to {e.Topic} now");
            }
		}

		private void OnRewardRedeemded(object sender, OnRewardRedeemedArgs e)
		{
			Console.WriteLine("Reward Redeemded:\n" +
				$"{e.ChannelId}|{e.DisplayName}|{e.Login}|{e.Message}|{e.RewardCost}" +
				$"|{e.RewardId}|{e.RewardPrompt}|{e.RewardTitle}");
			OnReward(this, e);
		}
	}
}
