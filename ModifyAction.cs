﻿using ObsPointsControl.Model;
using System;
using System.Windows.Forms;
using Action = ObsPointsControl.Model.Action;

namespace ObsPointsControl
{
	public partial class ModifyAction : Form
	{
		private Action action;
		public ModifyAction(ref Action action, bool isNew = true)
		{
			InitializeComponent();
			this.action = action;
			btnDelete.Enabled = !isNew;

			DialogResult = DialogResult.Cancel;
			btnSave.DialogResult = DialogResult.OK;
			btnDelete.DialogResult = DialogResult.Abort;
			btnCancel.DialogResult = DialogResult.Cancel;
		}

		private string ValidateValues()
		{
			if (txtTarget.TextLength < 1)
			{
				return "Target cannot be empty";
			}
			return null;
		}

		private void Store()
		{
			action.Type = cbType.SelectedItem.ToString().TTFromDisplayString() ?? throw new InvalidOperationException("an oject was unexpectetly null");
			action.Target = txtTarget.Text;
			action.Duration = (ulong)numDuration.Value;
			action.Timeout = (ulong)numTimeout.Value;
			action.Parent = action.Type switch
			{
				TargetType.Scene => txtSceneParent.Text.NullOnEmpty(),
				TargetType.Source => txtSourceParent.Text.NullOnEmpty(),
				_ => throw new ArgumentNullException()
			};
			action.Value = cbSourceIsActivate.Checked;
			action.PointCount = (uint)numCost.Value;
		}

		private void cbType_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch ((string)cbType.SelectedItem)
			{
				case "Scene":
					bxSources.Enabled = false;
					bxScenes.Enabled = true;
					break;
				case "Source":
					bxSources.Enabled = true;
					bxScenes.Enabled = false;
					break;
				default:
					break;
			}
		}

		private void ModifyAction_Load(object sender, EventArgs e)
		{
			lbId.Text = $"ID: {action.Id}";
			cbType.SelectedItem = action.Type.ToDisplayStrimg();
			txtTarget.Text = action.Target;
			numDuration.Value = action.Duration;
			numTimeout.Value = action.Timeout;
			txtSourceParent.Text = action.Type == TargetType.Source ? action.Parent : "";
			txtSceneParent.Text = action.Type == TargetType.Scene ? action.Parent : "";
			cbSourceIsActivate.Checked = action.Value;
			numCost.Value = action.PointCount;
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void ModifyAction_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
			{
				var validate = ValidateValues();
				if (validate != null)
				{
					MessageBox.Show("Unable to save:\n" + validate, "Invalid Options", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.Cancel = true;
					return;
				}
				Store();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
