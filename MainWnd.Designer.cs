﻿namespace ObsPointsControl
{
	partial class MainWnd
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tvStatus = new System.Windows.Forms.TreeView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lbLog = new System.Windows.Forms.ListBox();
			this.dataActions = new System.Windows.Forms.ListView();
			this.clHdPoints = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chHdAction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnConnectionSettings = new System.Windows.Forms.Button();
			this.btnAddAction = new System.Windows.Forms.Button();
			this.btnConnect = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tvStatus
			// 
			this.tvStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tvStatus.Location = new System.Drawing.Point(6, 19);
			this.tvStatus.Name = "tvStatus";
			this.tvStatus.PathSeparator = "|";
			this.tvStatus.Size = new System.Drawing.Size(313, 170);
			this.tvStatus.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.lbLog);
			this.groupBox1.Controls.Add(this.tvStatus);
			this.groupBox1.Location = new System.Drawing.Point(463, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(325, 426);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Status";
			// 
			// lbLog
			// 
			this.lbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbLog.FormattingEnabled = true;
			this.lbLog.Location = new System.Drawing.Point(7, 196);
			this.lbLog.Name = "lbLog";
			this.lbLog.Size = new System.Drawing.Size(312, 225);
			this.lbLog.TabIndex = 1;
			// 
			// dataActions
			// 
			this.dataActions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataActions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clHdPoints,
            this.chHdAction});
			this.dataActions.FullRowSelect = true;
			this.dataActions.GridLines = true;
			this.dataActions.HideSelection = false;
			this.dataActions.Location = new System.Drawing.Point(12, 41);
			this.dataActions.MultiSelect = false;
			this.dataActions.Name = "dataActions";
			this.dataActions.Size = new System.Drawing.Size(444, 397);
			this.dataActions.TabIndex = 2;
			this.dataActions.UseCompatibleStateImageBehavior = false;
			this.dataActions.View = System.Windows.Forms.View.Details;
			this.dataActions.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataActions_MouseDoubleClick);
			// 
			// clHdPoints
			// 
			this.clHdPoints.Text = "Points Required";
			this.clHdPoints.Width = 92;
			// 
			// chHdAction
			// 
			this.chHdAction.Text = "Actions";
			// 
			// btnConnectionSettings
			// 
			this.btnConnectionSettings.Location = new System.Drawing.Point(12, 12);
			this.btnConnectionSettings.Name = "btnConnectionSettings";
			this.btnConnectionSettings.Size = new System.Drawing.Size(125, 23);
			this.btnConnectionSettings.TabIndex = 3;
			this.btnConnectionSettings.Text = "Connection Settings";
			this.btnConnectionSettings.UseVisualStyleBackColor = true;
			this.btnConnectionSettings.Click += new System.EventHandler(this.btnConnectionSettings_Click);
			// 
			// btnAddAction
			// 
			this.btnAddAction.Location = new System.Drawing.Point(143, 12);
			this.btnAddAction.Name = "btnAddAction";
			this.btnAddAction.Size = new System.Drawing.Size(125, 23);
			this.btnAddAction.TabIndex = 4;
			this.btnAddAction.Text = "Add Action";
			this.btnAddAction.UseVisualStyleBackColor = true;
			this.btnAddAction.Click += new System.EventHandler(this.btnAddAction_Click);
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(331, 12);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(125, 23);
			this.btnConnect.TabIndex = 5;
			this.btnConnect.Text = "Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// MainWnd
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.btnAddAction);
			this.Controls.Add(this.btnConnectionSettings);
			this.Controls.Add(this.dataActions);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainWnd";
			this.Text = "Obs Point Control";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWnd_FormClosed);
			this.Load += new System.EventHandler(this.Main_Load);
			this.ResizeEnd += new System.EventHandler(this.MainWnd_ResizeEnd);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TreeView tvStatus;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListBox lbLog;
		private System.Windows.Forms.ListView dataActions;
		private System.Windows.Forms.ColumnHeader clHdPoints;
		private System.Windows.Forms.ColumnHeader chHdAction;
		private System.Windows.Forms.Button btnConnectionSettings;
		private System.Windows.Forms.Button btnAddAction;
		private System.Windows.Forms.Button btnConnect;
	}
}

