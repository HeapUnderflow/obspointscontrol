﻿using ObsPointsControl.Model;
using ObsPointsControl.Model.Obs;
using ObsPointsControl.Model.Twitch;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ObsPointsControl
{
	public partial class MainWnd : Form
	{
		public static Action<string> Log = v => Console.WriteLine("LOG: {0}", v);
		private readonly Dictionary<string, TreeNode> StatusNodes = new Dictionary<string, TreeNode>();
		private Config cfg;

		private ObsController obsController;
		private TwitchController twitchController;

		public bool connected = false;
		private bool paused = false;
		private KeyboardHook kbhook;

		public MainWnd()
		{
			// Emit logs to log-field instead of stdout
			if (Log != PushLogMessage) { Log = PushLogMessage; }
			InitializeComponent();
			dataActions.Columns[dataActions.Columns.Count - 1].Width = -2;
		}

		public void PushLogMessage(string msg)
		{
			Utils.SafeInvoke(lbLog, () =>
			{
				lbLog.Items.Insert(0, msg);
				if (lbLog.Items.Count > 100)
				{
					lbLog.Items.RemoveAt(lbLog.Items.Count - 1);
				}
			});
		}

		private void RebuilActionList()
		{
			dataActions.Items.Clear();
			foreach (var ac in cfg.Actions)
			{
				dataActions.Items.Add(new ListViewItem(
					new string[] {
						$"{ac.PointCount}",
						$"Type = {ac.Type.ToDisplayStrimg()}, " +
							$"Target = {ac.Target}, " +
							$"Parent = {ac.Parent ?? "[No Parent]"}, " +
							$"Duration = {ac.Duration}ms, " +
							$"Timeout = {ac.Timeout}ms, " +
							$"IsSet = {(ac.Value ? "Yes" : "No")}"
						}));
			}
		}

		private void Main_Load(object sender, EventArgs e)
		{
			cfg = Config.Load();
			StatusNodes.Add("st_obs", new TreeNode("Obs: Disconnected"));
			StatusNodes.Add("st_twitch", new TreeNode("Twitch: Disconnected"));
			StatusNodes.Add("cfg_obsaddr", 
				new TreeNode(
					string.Format(
						"Obs Address: {0}:{1}", 
						cfg.ObsAddr,
						cfg.ObsPort)));
			StatusNodes.Add("cfg_tw_user", new TreeNode($"Twitch User: {cfg.TwitchUser ?? "no user selected"}"));
			StatusNodes.Add("cfg_hk", new TreeNode($"Pause Hotkey: {cfg.Hotkey}"));
			StatusNodes.Add("st_paused", new TreeNode("Paused: " + (paused ? "Yes (paused)" : "No (running)")));

			tvStatus.Nodes.Add(
				new TreeNode("Service Status", new[] { StatusNodes["st_obs"], StatusNodes["st_twitch"], StatusNodes["st_paused"] }));
			tvStatus.Nodes.Add(
				new TreeNode("Configuration", new[] { StatusNodes["cfg_obsaddr"], StatusNodes["cfg_tw_user"], StatusNodes["cfg_hk"] }));

			tvStatus.ExpandAll();

			obsController = new ObsController(cfg.ObsAddr, cfg.ObsPort, cfg.ObsAuth);
			twitchController = new TwitchController(cfg.TwitchUserId, null);

			twitchController.OnConnected += (s, e) => Log("TWITCH: Connected");
			twitchController.OnDisconnected += (s, e) => Log("TWITCH: Disconnected");
			obsController.OnObsConnected += (s, e) => Log("OBS: Connected");
			obsController.OnObsDisconnected += (s, e) => Log("OBS: Disconnected");

			twitchController.OnConnected += (s, e) => Utils.SafeInvoke(tvStatus, () => StatusNodes["st_twitch"].Text = "Twitch: Connected");
			twitchController.OnDisconnected += (s, e) => Utils.SafeInvoke(tvStatus, () => StatusNodes["st_twitch"].Text = "Twitch: Disconnected");
			obsController.OnObsConnected += (s, e) => Utils.SafeInvoke(tvStatus, () => StatusNodes["st_obs"].Text = "Obs: Connected");
			obsController.OnObsDisconnected += (s, e) => Utils.SafeInvoke(tvStatus, () => StatusNodes["st_obs"].Text = "Obs: Disconnected");

			twitchController.OnReward += TwitchController_OnReward;

			obsController.SetPaused(paused);

			kbhook = new KeyboardHook();
			kbhook.RegisterHotKey(ObsPointsControl.ModifierKeys.Shift, Keys.F5);

			kbhook.KeyPressed += Kbhook_KeyPressed;

			RebuilActionList();
			Log("Startup Complete!");
		}

		private void Kbhook_KeyPressed(object sender, KeyPressedEventArgs e)
		{
			if (e.Modifier.HasFlag(ObsPointsControl.ModifierKeys.Shift) && e.Key.HasFlag(Keys.F5))
			{
				paused = !paused;
				obsController.SetPaused(paused);
				StatusNodes["st_paused"].Text = "Paused: " + (paused ? "Yes (paused)" : "No (running)");
			}
		}

		private void TwitchController_OnReward(object sender, TwitchLib.PubSub.Events.OnRewardRedeemedArgs e)
		{
			foreach (var act in cfg.Actions)
			{
				if (act.PointCount == e.RewardCost)
				{
					obsController.PushAction(act);
					break;
				}
			}
		}

		private void MainWnd_ResizeEnd(object sender, EventArgs e)
		{
			dataActions.Columns[dataActions.Columns.Count - 1].Width = -2;
		}

		private void UpdateState()
		{
			//StatusNodes["st_obs"].Text = $"Obs: {((bool)StatusNodes["st_obs"].Tag ? "Connected" : "Disconnected")}";
			//StatusNodes["st_twitch"].Text = $"Twitch: {((bool)StatusNodes["st_twitch"].Tag ? "Connected" : "Disconnected")}";
			StatusNodes["cfg_obsaddr"].Text = string.Format(
						"Obs Address: {0}:{1}",
						cfg.ObsAddr,
						cfg.ObsPort);
			StatusNodes["cfg_tw_user"].Text = $"Twitch User: {cfg.TwitchUser ?? "no user selected"}";
		}

		private void btnConnectionSettings_Click(object sender, EventArgs e)
		{
			var cs = new ConnectionSettings(ref cfg);
			cs.FormClosed += (s, e) =>
			{
				UpdateState();
				cfg.Store();
				obsController.Auth = cfg.ObsAuth;
				obsController.Url = cfg.ObsAddr;
				obsController.Port = cfg.ObsPort;
				twitchController.Auth = cfg.TwitchAuth;
				twitchController.Uid = cfg.TwitchUserId;
			};
			cs.ShowDialog();
		}

		private void MainWnd_FormClosed(object sender, FormClosedEventArgs e)
		{
			obsController.Disconnect();
			twitchController.Disconnect();
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			if (connected)
			{
				obsController.Disconnect();
				twitchController.Disconnect();
				btnConnect.Text = "Connect";
				connected = false;
			}
			else
			{
				obsController.Connect();
				twitchController.Connect();
				btnConnect.Text = "Disconnect";
				connected = true;
			}
		}

		private void btnAddAction_Click(object sender, EventArgs e)
		{
			var na = new Model.Action("", TargetType.Source, 1000);
			ModifyAction diag = new ModifyAction(ref na, true);
			if (diag.ShowDialog() == DialogResult.OK)
			{
				cfg.Actions.Add(na);
				cfg.Store();
				RebuilActionList();
			}
		}

		private void dataActions_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			int idx = dataActions.SelectedIndices[0];
			var action = cfg.Actions[idx];

			ModifyAction dialog = new ModifyAction(ref action, false);
			switch (dialog.ShowDialog())
			{
				case DialogResult.OK:
					action.Dbg();
					cfg.Store();
					RebuilActionList();
					break;
				case DialogResult.Abort:
					action.Dbg();
					cfg.Actions.RemoveAt(idx);
					cfg.Store();
					RebuilActionList();
					break;
				case DialogResult.Cancel:
					break;
				default:
					break;
			}
		}
	}
}
