---
author: HeapUnderflow
title: OBS Points Control
subtitle: The "howto" manual
toc: true
---

# OBS Points Control

The "how to make this shit work" document

## Pre-Setup

Okay, before you should even attempt to use this, you need a obs
plugin called `obs-websocket`. 
You can get it from here: [obs-websocket Download]

### Obs Websocket Setup

- Install obs-websocket
- Start OBS
- Go to `Tools > WebSocket Server Settings`
- In the popup window make shure that...
  - ...`Enable WebSockets server` is ticked
  - ...`Server Port` is set to 4444
  - ...`Enable authentication` is ticked
  - ...`Password` is set to a password 
    (make it a unique one like `ffaabb11223344`, 
    because it is not securely stored)
  - ...`Enable debug logging` is unticked
- Press OK to confirm changes

## Setting up OBS Points Control

- First order of bussines: start it
- You are now in the [Main Menu](#main-menu-ui)
- Click on `Connection Settings`
- Put in the required settings ([see here](#connection-settings-ui))
- Now you can configure actions using the add-action button ([see here](#add-action-ui))
- Finally click `Connect` and everything should be working


### Main Menu UI

Elements explained:

| Element                                        | Description                         |
| ------                                         | ----------                          |
| [Connection Settings](#connection-settings-ui) | General config options for the tool |
| [Add Action](#add-action-ui)                   | Create a new `Point Action`         |
| Connect                                        | Connect to OBS and Twitch           |
| Status                                         | Status information about the tool   |
| "Bottom Right Corner"                          | Event Log                           |
| "List"                                         | Lists all configured actions        |

To edit or delete an action, double-click it in the list

### Connection Settings UI

Elements explained:

| Group   | Element  | Description                                              |
| ------- | ---      | ----------                                               |
| Obs     | Adress   | The IP adress of OBS (usually `127.0.0.1`)               |
| Obs     | Port     | The Port you configured in OBS (usually `4444`)          |
| Obs     | Password | The Password you configured in OBS                       |
| Twitch  | Username | Your **lowercase** twitch username                       |
| Twitch  | Oauth    | The oauth token you aquired [here](#get-the-oauth-token) |

### Add Action UI

Elements explained:

| Applies to | Element   | Description                                                                                   |
| ---------- | -------   | -----------                                                                                   |
| Any        | Type      | The Type this Action applies to (`Source` or `Scene`)                                         |
| Any        | Target    | The `Target` this action applies to, this is the name of the element[^element]                |
| Any        | Cost      | How many points have to be redeemed for this to be triggered                                  |
| Any        | Duration  | How long the element[^element] stays active (in milliseconds)                                 |
| Any        | Timeout   | The time (in milliseconds) the element[^element] is unavaliable after activation)             |
| Sources    | Parent    | The `parent`-scene of the source (optional, avoids issues if primary scene is often switched) |
| Sources    | Activate? | If checked the specified source is set visible on event, otherwise it is set invisible        |
| Scenes     | Parent    | The scene to return to, if unset the currently active scene is used                           |

[^element]: Element here describes either a `Source` or a `Scene`

### Get the OAuth token

- Go to [https://twitchapps.com/tokengen/](https://twitchapps.com/tokengen/)
- As **Client ID** put in `vde21u18dna92iwn9z5yzmyqwdpthq`
- As **Scopes** put in `channel:read:redemptions`
- Click on the button below **Redirect URL** (may be a broken image icon)
- Copy the token displayed into the `OAuth` box in the `Connection Settings` menu

## The ugly truth

Due to lack of time this has some really interesting issues that one should be aware of:

- Never, ever, ever, ever click `Connect` without OBS running. It locks up the interface and you have to
  kill it using the task manager
- I have not been able to test the recieving of redemption events at all, so you **need** to do some test
  before you use it on live.
- When you "pause" using the hotkey, it will still continue processing the currently running event (if any)
- The pause hotkey is `Shift+F5` and cannot be currently changed


[obs-websocket Download]: https://github.com/Palakis/obs-websocket/releases/tag/4.7.0
