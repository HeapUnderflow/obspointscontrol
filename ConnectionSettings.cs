﻿using ObsPointsControl.Model;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace ObsPointsControl
{
	public partial class ConnectionSettings : Form
	{
		private Config config;
		public ConnectionSettings(ref Config config)
		{
			this.config = config;
			InitializeComponent();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (!verify())
			{
				return;
			}

			config.ObsAddr = txtObsAddr.Text;
			config.ObsPort = (ushort)numObsPort.Value;
			config.ObsAuth = txtObsAuth.Text.Length > 0 ? txtObsAuth.Text : null;

			config.TwitchUser = txtTwitchUser.Text.Length > 0 ? txtTwitchUser.Text : null;
			config.TwitchAuth = txtTwitchAuth.Text.Length > 0 ? txtTwitchAuth.Text : null;
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private bool verify()
		{
			try
			{
				new IPAddress(txtObsAddr.Text.Split('.').Select(v => byte.Parse(v)).ToArray());
			}
			catch (ArgumentException e)
			{
				MessageBox.Show(string.Format("Invalid Ip format:\n{0}", e.Message));
				return false;
			}
			catch (FormatException e) 
			{
				MessageBox.Show(string.Format("Invalid number format:\n{0}", e.Message));
				return false;
			}
			catch (OverflowException e)
			{
				MessageBox.Show(string.Format("The number did not fit into a byte:\n{0}", e.Message));
				return false;
			}

			return true;
		}

		private void ConnectionSettings_Load(object sender, EventArgs e)
		{
			txtObsAddr.Text = config.ObsAddr;
			numObsPort.Value = config.ObsPort;
			txtObsAuth.Text = config.ObsAuth;

			txtTwitchUser.Text = config.TwitchUser;
			txtTwitchAuth.Text = config.TwitchAuth;
		}

		private void ConnectionSettings_FormClosing(object sender, FormClosingEventArgs e)
		{

		}
	}
}
