﻿namespace ObsPointsControl
{
	partial class ModifyAction
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbId = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.txtTarget = new System.Windows.Forms.TextBox();
			this.cbType = new System.Windows.Forms.ComboBox();
			this.lbType = new System.Windows.Forms.Label();
			this.lbTarget = new System.Windows.Forms.Label();
			this.lbSourceParent = new System.Windows.Forms.Label();
			this.bxGeneral = new System.Windows.Forms.GroupBox();
			this.lbPoints = new System.Windows.Forms.Label();
			this.numCost = new System.Windows.Forms.NumericUpDown();
			this.lbCost = new System.Windows.Forms.Label();
			this.numTimeout = new System.Windows.Forms.NumericUpDown();
			this.numDuration = new System.Windows.Forms.NumericUpDown();
			this.lbMs2 = new System.Windows.Forms.Label();
			this.lbMs1 = new System.Windows.Forms.Label();
			this.lbTimeout = new System.Windows.Forms.Label();
			this.lbDuration = new System.Windows.Forms.Label();
			this.bxScenes = new System.Windows.Forms.GroupBox();
			this.txtSceneParent = new System.Windows.Forms.TextBox();
			this.lbSceneParent = new System.Windows.Forms.Label();
			this.bxSources = new System.Windows.Forms.GroupBox();
			this.cbSourceIsActivate = new System.Windows.Forms.CheckBox();
			this.txtSourceParent = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.bxGeneral.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numDuration)).BeginInit();
			this.bxScenes.SuspendLayout();
			this.bxSources.SuspendLayout();
			this.SuspendLayout();
			// 
			// lbId
			// 
			this.lbId.AutoSize = true;
			this.lbId.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbId.Location = new System.Drawing.Point(8, 87);
			this.lbId.Name = "lbId";
			this.lbId.Size = new System.Drawing.Size(168, 14);
			this.lbId.TabIndex = 0;
			this.lbId.Text = "Id: 0000-0000-0000-0000";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(12, 212);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(141, 23);
			this.btnSave.TabIndex = 1;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// txtTarget
			// 
			this.txtTarget.Location = new System.Drawing.Point(54, 46);
			this.txtTarget.Name = "txtTarget";
			this.txtTarget.Size = new System.Drawing.Size(186, 20);
			this.txtTarget.TabIndex = 2;
			// 
			// cbType
			// 
			this.cbType.FormattingEnabled = true;
			this.cbType.Items.AddRange(new object[] {
            "Source",
            "Scene"});
			this.cbType.Location = new System.Drawing.Point(54, 19);
			this.cbType.Name = "cbType";
			this.cbType.Size = new System.Drawing.Size(186, 21);
			this.cbType.TabIndex = 4;
			this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
			// 
			// lbType
			// 
			this.lbType.AutoSize = true;
			this.lbType.Location = new System.Drawing.Point(8, 19);
			this.lbType.Name = "lbType";
			this.lbType.Size = new System.Drawing.Size(31, 13);
			this.lbType.TabIndex = 5;
			this.lbType.Text = "Type";
			// 
			// lbTarget
			// 
			this.lbTarget.AutoSize = true;
			this.lbTarget.Location = new System.Drawing.Point(8, 49);
			this.lbTarget.Name = "lbTarget";
			this.lbTarget.Size = new System.Drawing.Size(38, 13);
			this.lbTarget.TabIndex = 6;
			this.lbTarget.Text = "Target";
			// 
			// lbSourceParent
			// 
			this.lbSourceParent.AutoSize = true;
			this.lbSourceParent.Location = new System.Drawing.Point(6, 19);
			this.lbSourceParent.Name = "lbSourceParent";
			this.lbSourceParent.Size = new System.Drawing.Size(38, 13);
			this.lbSourceParent.TabIndex = 7;
			this.lbSourceParent.Text = "Parent";
			// 
			// bxGeneral
			// 
			this.bxGeneral.Controls.Add(this.lbPoints);
			this.bxGeneral.Controls.Add(this.numCost);
			this.bxGeneral.Controls.Add(this.lbCost);
			this.bxGeneral.Controls.Add(this.numTimeout);
			this.bxGeneral.Controls.Add(this.numDuration);
			this.bxGeneral.Controls.Add(this.lbMs2);
			this.bxGeneral.Controls.Add(this.lbMs1);
			this.bxGeneral.Controls.Add(this.lbTimeout);
			this.bxGeneral.Controls.Add(this.lbDuration);
			this.bxGeneral.Controls.Add(this.cbType);
			this.bxGeneral.Controls.Add(this.lbId);
			this.bxGeneral.Controls.Add(this.lbTarget);
			this.bxGeneral.Controls.Add(this.txtTarget);
			this.bxGeneral.Controls.Add(this.lbType);
			this.bxGeneral.Location = new System.Drawing.Point(12, 12);
			this.bxGeneral.Name = "bxGeneral";
			this.bxGeneral.Size = new System.Drawing.Size(506, 112);
			this.bxGeneral.TabIndex = 8;
			this.bxGeneral.TabStop = false;
			this.bxGeneral.Text = "General";
			// 
			// lbPoints
			// 
			this.lbPoints.AutoSize = true;
			this.lbPoints.Location = new System.Drawing.Point(459, 16);
			this.lbPoints.Name = "lbPoints";
			this.lbPoints.Size = new System.Drawing.Size(35, 13);
			this.lbPoints.TabIndex = 17;
			this.lbPoints.Text = "points";
			// 
			// numCost
			// 
			this.numCost.Location = new System.Drawing.Point(295, 12);
			this.numCost.Maximum = new decimal(new int[] {
            86400000,
            0,
            0,
            0});
			this.numCost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCost.Name = "numCost";
			this.numCost.Size = new System.Drawing.Size(158, 20);
			this.numCost.TabIndex = 16;
			this.numCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// lbCost
			// 
			this.lbCost.AutoSize = true;
			this.lbCost.Location = new System.Drawing.Point(246, 14);
			this.lbCost.Name = "lbCost";
			this.lbCost.Size = new System.Drawing.Size(28, 13);
			this.lbCost.TabIndex = 15;
			this.lbCost.Text = "Cost";
			// 
			// numTimeout
			// 
			this.numTimeout.Location = new System.Drawing.Point(295, 65);
			this.numTimeout.Maximum = new decimal(new int[] {
            86400000,
            0,
            0,
            0});
			this.numTimeout.Name = "numTimeout";
			this.numTimeout.Size = new System.Drawing.Size(158, 20);
			this.numTimeout.TabIndex = 14;
			// 
			// numDuration
			// 
			this.numDuration.Location = new System.Drawing.Point(295, 38);
			this.numDuration.Maximum = new decimal(new int[] {
            86400000,
            0,
            0,
            0});
			this.numDuration.Name = "numDuration";
			this.numDuration.Size = new System.Drawing.Size(158, 20);
			this.numDuration.TabIndex = 13;
			// 
			// lbMs2
			// 
			this.lbMs2.AutoSize = true;
			this.lbMs2.Location = new System.Drawing.Point(459, 67);
			this.lbMs2.Name = "lbMs2";
			this.lbMs2.Size = new System.Drawing.Size(20, 13);
			this.lbMs2.TabIndex = 12;
			this.lbMs2.Text = "ms";
			// 
			// lbMs1
			// 
			this.lbMs1.AutoSize = true;
			this.lbMs1.Location = new System.Drawing.Point(459, 40);
			this.lbMs1.Name = "lbMs1";
			this.lbMs1.Size = new System.Drawing.Size(20, 13);
			this.lbMs1.TabIndex = 11;
			this.lbMs1.Text = "ms";
			// 
			// lbTimeout
			// 
			this.lbTimeout.AutoSize = true;
			this.lbTimeout.Location = new System.Drawing.Point(244, 64);
			this.lbTimeout.Name = "lbTimeout";
			this.lbTimeout.Size = new System.Drawing.Size(45, 13);
			this.lbTimeout.TabIndex = 8;
			this.lbTimeout.Text = "Timeout";
			// 
			// lbDuration
			// 
			this.lbDuration.AutoSize = true;
			this.lbDuration.Location = new System.Drawing.Point(244, 40);
			this.lbDuration.Name = "lbDuration";
			this.lbDuration.Size = new System.Drawing.Size(47, 13);
			this.lbDuration.TabIndex = 7;
			this.lbDuration.Text = "Duration";
			// 
			// bxScenes
			// 
			this.bxScenes.Controls.Add(this.txtSceneParent);
			this.bxScenes.Controls.Add(this.lbSceneParent);
			this.bxScenes.Location = new System.Drawing.Point(268, 130);
			this.bxScenes.Name = "bxScenes";
			this.bxScenes.Size = new System.Drawing.Size(250, 76);
			this.bxScenes.TabIndex = 9;
			this.bxScenes.TabStop = false;
			this.bxScenes.Text = "Scenes Only";
			// 
			// txtSceneParent
			// 
			this.txtSceneParent.Location = new System.Drawing.Point(50, 16);
			this.txtSceneParent.Name = "txtSceneParent";
			this.txtSceneParent.Size = new System.Drawing.Size(186, 20);
			this.txtSceneParent.TabIndex = 9;
			// 
			// lbSceneParent
			// 
			this.lbSceneParent.AutoSize = true;
			this.lbSceneParent.Location = new System.Drawing.Point(6, 19);
			this.lbSceneParent.Name = "lbSceneParent";
			this.lbSceneParent.Size = new System.Drawing.Size(38, 13);
			this.lbSceneParent.TabIndex = 10;
			this.lbSceneParent.Text = "Parent";
			// 
			// bxSources
			// 
			this.bxSources.Controls.Add(this.cbSourceIsActivate);
			this.bxSources.Controls.Add(this.txtSourceParent);
			this.bxSources.Controls.Add(this.lbSourceParent);
			this.bxSources.Location = new System.Drawing.Point(12, 130);
			this.bxSources.Name = "bxSources";
			this.bxSources.Size = new System.Drawing.Size(250, 76);
			this.bxSources.TabIndex = 10;
			this.bxSources.TabStop = false;
			this.bxSources.Text = "Sources Only";
			// 
			// cbSourceIsActivate
			// 
			this.cbSourceIsActivate.AutoSize = true;
			this.cbSourceIsActivate.Checked = true;
			this.cbSourceIsActivate.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbSourceIsActivate.Location = new System.Drawing.Point(7, 46);
			this.cbSourceIsActivate.Name = "cbSourceIsActivate";
			this.cbSourceIsActivate.Size = new System.Drawing.Size(71, 17);
			this.cbSourceIsActivate.TabIndex = 8;
			this.cbSourceIsActivate.Text = "Activate?";
			this.cbSourceIsActivate.UseVisualStyleBackColor = true;
			// 
			// txtSourceParent
			// 
			this.txtSourceParent.Location = new System.Drawing.Point(50, 16);
			this.txtSourceParent.Name = "txtSourceParent";
			this.txtSourceParent.Size = new System.Drawing.Size(186, 20);
			this.txtSourceParent.TabIndex = 7;
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(375, 212);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(143, 23);
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Enabled = false;
			this.btnDelete.Location = new System.Drawing.Point(159, 211);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(64, 23);
			this.btnDelete.TabIndex = 12;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// ModifyAction
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(527, 245);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.bxSources);
			this.Controls.Add(this.bxScenes);
			this.Controls.Add(this.bxGeneral);
			this.Controls.Add(this.btnSave);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ModifyAction";
			this.Text = "ModifyAction";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModifyAction_FormClosing);
			this.Load += new System.EventHandler(this.ModifyAction_Load);
			this.bxGeneral.ResumeLayout(false);
			this.bxGeneral.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numDuration)).EndInit();
			this.bxScenes.ResumeLayout(false);
			this.bxScenes.PerformLayout();
			this.bxSources.ResumeLayout(false);
			this.bxSources.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lbId;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox txtTarget;
		private System.Windows.Forms.ComboBox cbType;
		private System.Windows.Forms.Label lbType;
		private System.Windows.Forms.Label lbTarget;
		private System.Windows.Forms.Label lbSourceParent;
		private System.Windows.Forms.GroupBox bxGeneral;
		private System.Windows.Forms.GroupBox bxScenes;
		private System.Windows.Forms.GroupBox bxSources;
		private System.Windows.Forms.TextBox txtSourceParent;
		private System.Windows.Forms.CheckBox cbSourceIsActivate;
		private System.Windows.Forms.Label lbMs2;
		private System.Windows.Forms.Label lbMs1;
		private System.Windows.Forms.Label lbTimeout;
		private System.Windows.Forms.Label lbDuration;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtSceneParent;
		private System.Windows.Forms.Label lbSceneParent;
		private System.Windows.Forms.NumericUpDown numTimeout;
		private System.Windows.Forms.NumericUpDown numDuration;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Label lbPoints;
		private System.Windows.Forms.NumericUpDown numCost;
		private System.Windows.Forms.Label lbCost;
	}
}