﻿namespace ObsPointsControl
{
	partial class ConnectionSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.vdwadwad = new System.Windows.Forms.Label();
			this.txtObsAuth = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.numObsPort = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.txtObsAddr = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.lbOauth = new System.Windows.Forms.Label();
			this.txtTwitchAuth = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtTwitchUser = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numObsPort)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.vdwadwad);
			this.groupBox1.Controls.Add(this.txtObsAuth);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.numObsPort);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.txtObsAddr);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(230, 106);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Obs";
			// 
			// vdwadwad
			// 
			this.vdwadwad.AutoSize = true;
			this.vdwadwad.Location = new System.Drawing.Point(6, 78);
			this.vdwadwad.Name = "vdwadwad";
			this.vdwadwad.Size = new System.Drawing.Size(53, 13);
			this.vdwadwad.TabIndex = 5;
			this.vdwadwad.Text = "Password";
			// 
			// txtObsAuth
			// 
			this.txtObsAuth.Location = new System.Drawing.Point(60, 75);
			this.txtObsAuth.Name = "txtObsAuth";
			this.txtObsAuth.Size = new System.Drawing.Size(164, 20);
			this.txtObsAuth.TabIndex = 4;
			this.txtObsAuth.UseSystemPasswordChar = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 46);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(26, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Port";
			// 
			// numObsPort
			// 
			this.numObsPort.Location = new System.Drawing.Point(60, 46);
			this.numObsPort.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
			this.numObsPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numObsPort.Name = "numObsPort";
			this.numObsPort.Size = new System.Drawing.Size(164, 20);
			this.numObsPort.TabIndex = 2;
			this.numObsPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Address";
			// 
			// txtObsAddr
			// 
			this.txtObsAddr.Location = new System.Drawing.Point(60, 13);
			this.txtObsAddr.Name = "txtObsAddr";
			this.txtObsAddr.Size = new System.Drawing.Size(164, 20);
			this.txtObsAddr.TabIndex = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.lbOauth);
			this.groupBox3.Controls.Add(this.txtTwitchAuth);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Controls.Add(this.txtTwitchUser);
			this.groupBox3.Location = new System.Drawing.Point(12, 124);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(230, 75);
			this.groupBox3.TabIndex = 1;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Twitch";
			// 
			// lbOauth
			// 
			this.lbOauth.AutoSize = true;
			this.lbOauth.Location = new System.Drawing.Point(6, 43);
			this.lbOauth.Name = "lbOauth";
			this.lbOauth.Size = new System.Drawing.Size(36, 13);
			this.lbOauth.TabIndex = 3;
			this.lbOauth.Text = "Oauth";
			// 
			// txtTwitchAuth
			// 
			this.txtTwitchAuth.Location = new System.Drawing.Point(67, 40);
			this.txtTwitchAuth.Name = "txtTwitchAuth";
			this.txtTwitchAuth.Size = new System.Drawing.Size(157, 20);
			this.txtTwitchAuth.TabIndex = 2;
			this.txtTwitchAuth.UseSystemPasswordChar = true;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(55, 13);
			this.label3.TabIndex = 1;
			this.label3.Text = "Username";
			// 
			// txtTwitchUser
			// 
			this.txtTwitchUser.Location = new System.Drawing.Point(67, 13);
			this.txtTwitchUser.Name = "txtTwitchUser";
			this.txtTwitchUser.Size = new System.Drawing.Size(157, 20);
			this.txtTwitchUser.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(167, 205);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(12, 205);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 3;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// ConnectionSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(250, 236);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ConnectionSettings";
			this.Text = "ConnectionSettings";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConnectionSettings_FormClosing);
			this.Load += new System.EventHandler(this.ConnectionSettings_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numObsPort)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label vdwadwad;
		private System.Windows.Forms.TextBox txtObsAuth;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numObsPort;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtObsAddr;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtTwitchUser;
		private System.Windows.Forms.Label lbOauth;
		private System.Windows.Forms.TextBox txtTwitchAuth;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
	}
}